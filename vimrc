call pathogen#infect()

set nocompatible
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

syntax on
filetype plugin indent on

nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>
